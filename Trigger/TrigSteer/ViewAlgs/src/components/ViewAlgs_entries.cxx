

#include "../EventViewCreatorAlgorithm.h"
#include "../EventViewCreatorAlgorithmWithJets.h"
#include "../EventViewCreatorAlgorithmWithMuons.h"
#include "../MergeViews.h"



DECLARE_COMPONENT( EventViewCreatorAlgorithm )
DECLARE_COMPONENT( EventViewCreatorAlgorithmWithJets )
DECLARE_COMPONENT( EventViewCreatorAlgorithmWithMuons )
DECLARE_COMPONENT( MergeViews )

