/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "AthenaKernel/getMessageSvc.h"

namespace TrigCompositeUtils {

  /**
   * @brief Creates and right away records the Container CONT with the key.
   * No Aux store.
   * Returns the WriteHandle. 
   * If possible provide the context that comes via an argument to execute otherwise it will default to looking it up which is slower.
   **/
  template<class CONT>
  SG::WriteHandle<CONT> createAndStoreNoAux( const SG::WriteHandleKey<CONT>& key, const EventContext& ctx ) {
    SG::WriteHandle<CONT> handle( key, ctx );
    auto data = std::make_unique<CONT>() ;
    if (handle.record( std::move( data ) ).isFailure()) {
      throw std::runtime_error( "ERROR in TrigCompositeUtils::createAndStoreNoAux Unable to record " + key.key());
    }
    return handle;
  }

  /**
   * @brief Creates and right away records the Container CONT with the key.
   * With Aux store.
   * Returns the WriteHandle. 
   * If possible provide the context that comes via an argument to execute otherwise it will default to looking it up which is slower.
   **/
  template<class CONT, class AUX>
  SG::WriteHandle<CONT> createAndStoreWithAux( const SG::WriteHandleKey<CONT>& key, const EventContext& ctx ) {
    SG::WriteHandle<CONT> handle( key, ctx );
    auto data = std::make_unique<CONT>() ;
    auto aux = std::make_unique<AUX>() ;
    data->setStore( aux.get() );
    if (handle.record( std::move( data ), std::move( aux )  ).isFailure()) {
      throw std::runtime_error( "ERROR in TrigCompositeUtils::createAndStoreWithAux Unable to record " + key.key());
    }
    return handle;
  }

  template<typename T>
  void
  findLinks(const xAOD::TrigComposite* start, const std::string& linkName, std::vector<LinkInfo<T>>& links) {
    ElementLinkVector<T> featureLinks;
    if (start->hasObjectCollectionLinks(linkName, ClassID_traits<T>::ID())) {
      featureLinks = start->objectCollectionLinks<T>(linkName);
    }
    if (start->hasObjectLink(linkName, ClassID_traits<T>::ID())) {
      featureLinks.push_back(start->objectLink<T>(linkName));
    }
    for (const ElementLink<T>& featureLink : featureLinks) {
      // Check for duplicates
      if (std::none_of(links.begin(), links.end(), [&](const auto& li) { return (li.link == featureLink); } ))
      {
        links.emplace_back(start, featureLink);
      }
    }
    // Recursive
    for (const auto& seed : getLinkToPrevious(start)) {
      findLinks<T>(*seed, linkName, links);
    }
  }

  template<typename T>
  std::vector<LinkInfo<T>>
  findLinks(const xAOD::TrigComposite* start, const std::string& linkName) {
    std::vector<LinkInfo<T>> links;
    findLinks(start, linkName, links);
    return links;
  }

  template<typename T>
  LinkInfo<T>
  findLink(const xAOD::TrigComposite* start, const std::string& linkName, const bool suppressMultipleLinksWarning) {
    std::vector<LinkInfo<T>> links = findLinks<T>(start, linkName);
    if (links.size() > 1 && !suppressMultipleLinksWarning) {
      MsgStream(Athena::getMessageSvc(), "TrigCompositeUtils::findLink") << MSG::WARNING
        << links.size() << " links found for " << linkName
        << " returning the first link, consider using findLinks." << endmsg;
    }
    if (links.size() > 0) {
      return links.at(0);
    }
    return LinkInfo<T>(); // invalid link
  }

  template<class CONTAINER>
  const std::vector< LinkInfo<CONTAINER> > getFeaturesOfType(
    const std::vector<ElementLinkVector<DecisionContainer>>& linkVector,
    const bool oneFeaturePerLeg,
    const std::string& featureName,
    const DecisionIDContainer chainIDs) {

    std::vector< LinkInfo<CONTAINER> > features;
    // For each unique path through the navigation for a given chain
    for (const ElementLinkVector<DecisionContainer>& decisionPath : linkVector) {
      // For each step along this path, starting at the terminus and working back towards L1
      for (const ElementLink<DecisionContainer>& decisionObjLink : decisionPath) {
        const Decision* decisionObj = (*decisionObjLink);
        ElementLinkVector<CONTAINER> featureLinks;

        // Look up what named links are available in the Decision Object
        std::vector<std::string> availableLinkNames;
        if (featureName == "") { 
          const std::vector<std::string> getSingleLinkNames = decisionObj->getObjectNames<CONTAINER>();
          const std::vector<std::string> getCollectionLinkNames = decisionObj->getObjectCollectionNames<CONTAINER>();
          std::copy(getSingleLinkNames.begin(), getSingleLinkNames.end(), std::back_inserter(availableLinkNames));
          std::copy(getCollectionLinkNames.begin(), getCollectionLinkNames.end(), std::back_inserter(availableLinkNames));
        } else { // Just looking for an explicitly named feature
          availableLinkNames.push_back( featureName );
        }

        // Fetch the named links that we're interested in
        for (const std::string& featureNameToGet : availableLinkNames) {
          // This try block protects against ExcCLIDMismatch throws from
          // features which do not derive from IParticle, when an IParticle interface is requested.
          try {
            // Slices may have added link collections
            if (decisionObj->hasObjectCollectionLinks(featureNameToGet, ClassID_traits< CONTAINER >::ID())) {
              ElementLinkVector<CONTAINER> collectionLinks = decisionObj->objectCollectionLinks<CONTAINER>( featureNameToGet );
              std::copy(collectionLinks.begin(), collectionLinks.end(), std::back_inserter(featureLinks));
            }
          } catch (SG::ExcCLIDMismatch&) {
            // This is in place to catch the exception caused by non-IParticle features when an IParticle interface is requested.
            // We're fine to catch this silently and cary on looking at the next Decision object in the graph
          }
          try {
            // Slices may have added single links. Note: the framework-specified "feature" link is always a single link.
            if (decisionObj->hasObjectLink(featureNameToGet, ClassID_traits< CONTAINER >::ID())) {
              featureLinks.push_back( decisionObj->objectLink<CONTAINER>( featureNameToGet ) );
            }
          } catch (SG::ExcCLIDMismatch&) {
            // Silently. As above.
          }
        }

        // Check if the Decsision object is active for a specific set of Chains-of-interest (as supplied by the TDT)
        ActiveState state = ActiveState::UNSET;
        if (chainIDs.size() > 0) {
          // If we were given a list of chains to consider then we start assuming none passed this decisionObj
          state = ActiveState::INACTIVE;
          for (DecisionID id : chainIDs) {
            if (std::count(decisionObj->decisions().begin(), decisionObj->decisions().end(), id) == 1) {
              state = ActiveState::ACTIVE;
              break;
            }
          }
        }

        // Copy the fetched links into the return vector
        for (const ElementLink<CONTAINER>& featureLink : featureLinks) {
          typename std::vector<LinkInfo<CONTAINER>>::iterator vecIt = std::find_if(features.begin(), features.end(), [&](const auto& li) { return li.link == featureLink; } );
          if (vecIt == features.end()) {
            // Link did not already exist - add it to the output
            features.emplace_back( decisionObj, featureLink, state );
          } else {
            // Link already existed - if the link's state in the return vector is INACTIVE but is ACTIVE here,
            // then we need to change it to ACTIVE as this denotes one-or-more of the requested chains were active for the object.
            if (vecIt->state == ActiveState::INACTIVE) {
              vecIt->state = ActiveState::ACTIVE;
            }
          }
        }

        // Stop processing this path through the navigation if the oneFeaturePerLeg flag is set
        if (featureLinks.size() && oneFeaturePerLeg) {
          break;
        }

      } // for (decisionLink : decisionPath)
    } // for (decisionPath : linkVector)
    return features;
  }

}
